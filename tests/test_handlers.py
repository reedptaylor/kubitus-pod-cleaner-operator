# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from logging import Logger, LoggerAdapter
from typing import Union
from unittest.mock import Mock, call, patch

import kopf

from kubitus_pod_cleaner_operator.handlers import pod_event


def run_pod_event(body: kopf.Body, logger: Union[Logger, LoggerAdapter]) -> None:
    """
    Run pod_event handler.

    Args:
        body: Full resource.
        logger: Logger.
    """
    pod_event(  # type: ignore
        body=body,
        spec=body.spec,
        meta=body.meta,
        status=body.status,
        uid=body.metadata.uid,
        name=body.metadata.name,
        namespace=body.metadata.namespace,
        labels=body.metadata.labels,
        annotations=body.metadata.annotations,
        logger=logger,
    )


def test_pod_event_empty_status() -> None:
    """Test pod_event handler, empty status."""
    logger = Mock()
    body = kopf.Body({
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': 'my-pod',
        },
        'status': {},
    })
    run_pod_event(body, logger)
    assert not logger.mock_calls


def test_pod_event_notready_status() -> None:
    """Test pod_event handler, NotReady status."""
    logger = Mock()
    body = kopf.Body({
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': 'my-pod',
            'namespace': 'myns',
        },
        'status': {
            'containerStatuses': [
                {
                    'containerID': '...',
                    'image': '...',
                    'imageID': '...',
                    'lastState': {},
                    'name': 'main',
                    'ready': False,
                    'restartCount': 0,
                    'started': False,
                    'state': {
                        'terminated': {
                            'containerID': '...',
                            'exitCode': 0,
                            'finishedAt': '2022-02-02T19:46:34Z',
                            'reason': 'Completed',
                            'startedAt': '2022-02-02T19:46:34Z',
                        },
                    },
                },
                {
                    'containerID': '...',
                    'image': 'docker.io/istio/proxyv2:1.12.2',
                    'imageID': '...',
                    'lastState': {},
                    'name': 'istio-proxy',
                    'ready': True,
                    'restartCount': 0,
                    'started': True,
                    'state': {
                        'running': {
                            'startedAt': '2022-02-02T19:46:32Z',
                        },
                    },
                },
            ],
            'phase': 'Running',
        },
    })
    with patch('kubitus_pod_cleaner_operator.handlers.kopf', autospec=True) as mocked_kopf:
        with patch('kubitus_pod_cleaner_operator.handlers.core_v1_api') as mocked_core_v1_api:
            with patch('kubitus_pod_cleaner_operator.handlers.stream') as mocked_stream:
                run_pod_event(body, logger)
                assert mocked_kopf.mock_calls == [
                    call.info(body, reason='IstioProxyStopping', message='Stopping istio-proxy'),
                    call.info(body, reason='IstioProxyStopped', message='Stopped istio-proxy'),
                ]
                assert mocked_core_v1_api.mock_calls == [
                    call.CoreV1Api(),
                ]
                assert mocked_stream.mock_calls == [
                    call(
                        mocked_core_v1_api.CoreV1Api().connect_get_namespaced_pod_exec,
                        'my-pod',
                        'myns',
                        container='istio-proxy',
                        command=['curl', '-X', 'POST', 'http://localhost:15000/quitquitquit'],
                        stderr=True,
                        stdin=False,
                        stdout=True,
                        tty=False,
                    ),
                ]
    assert logger.mock_calls == [
        call.info('Stopping istio-proxy'),
        call.info('Stopped istio-proxy'),
    ]


def test_pod_event_not_on_istio() -> None:
    """Test pod_event handler on pod without istio-proxy."""
    logger = Mock()
    body = kopf.Body({
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': 'my-pod',
            'namespace': 'myns',
        },
        'status': {
            'containerStatuses': [
                {
                    'containerID': '...',
                    'image': '...',
                    'imageID': '...',
                    'lastState': {},
                    'name': 'main',
                    'ready': False,
                    'restartCount': 0,
                    'started': False,
                    'state': {
                        'terminated': {
                            'containerID': '...',
                            'exitCode': 0,
                            'finishedAt': '2022-02-02T19:46:34Z',
                            'reason': 'Completed',
                            'startedAt': '2022-02-02T19:46:34Z',
                        },
                    },
                },
            ],
            'phase': 'Running',
        },
    })
    with patch('kubitus_pod_cleaner_operator.handlers.kopf', autospec=True) as mocked_kopf:
        with patch('kubitus_pod_cleaner_operator.handlers.core_v1_api') as mocked_core_v1_api:
            with patch('kubitus_pod_cleaner_operator.handlers.stream') as mocked_stream:
                run_pod_event(body, logger)
                assert not mocked_kopf.mock_calls
                assert not mocked_core_v1_api.mock_calls
                assert not mocked_stream.mock_calls
    assert not logger.mock_calls


def test_pod_event_error() -> None:
    """Test pod_event handler on pod with error."""
    logger = Mock()
    # https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/-/issues/5#note_1101278594
    body = kopf.Body({
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': 'my-pod',
            'namespace': 'myns',
        },
        'status': {
            'containerStatuses': [
                {
                    'containerID': '...',
                    'image': '...',
                    'imageID': '...',
                    'lastState': {},
                    'name': 'application',
                    'ready': False,
                    'restartCount': 0,
                    'started': False,
                    'state': {
                        'terminated': {
                            'containerID': '...',
                            'exitCode': 132,
                            'finishedAt': '2022-09-15T00:19:01Z',
                            'reason': 'Error',
                            'startedAt': '2022-09-15T00:18:47Z',
                        },
                    },
                },
                {
                    'containerID': '...',
                    'image': 'docker.io/istio/proxyv2:1.12.2',
                    'imageID': '...',
                    'lastState': {},
                    'name': 'istio-proxy',
                    'ready': True,
                    'restartCount': 0,
                    'started': True,
                    'state': {
                        'running': {
                            'startedAt': '2022-09-15T00:18:41Z',
                        },
                    },
                },
            ],
            'phase': 'Running',
        },
    })
    with patch('kubitus_pod_cleaner_operator.handlers.kopf', autospec=True) as mocked_kopf:
        with patch('kubitus_pod_cleaner_operator.handlers.core_v1_api') as mocked_core_v1_api:
            with patch('kubitus_pod_cleaner_operator.handlers.stream') as mocked_stream:
                run_pod_event(body, logger)
                assert not mocked_kopf.mock_calls
                assert not mocked_core_v1_api.mock_calls
                assert not mocked_stream.mock_calls
    assert not logger.mock_calls


def test_pod_event_crashloopbackoff() -> None:
    """Test pod_event handler on pod CrashLoopBackOff-ing."""
    logger = Mock()
    # https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/-/issues/5#note_1111395123
    body = kopf.Body({
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': 'my-pod',
            'namespace': 'myns',
        },
        'status': {
            'containerStatuses': [
                {
                    'containerID': '...',
                    'image': '...',
                    'imageID': '...',
                    'lastState': {},
                    'name': 'application',
                    'ready': False,
                    'restartCount': 1,
                    'started': False,
                    'state': {
                        'waiting': {
                            'message': 'back-off 10s restarting failed container=application pod=<redacted>',
                            'reason': 'CrashLoopBackOff',
                        },
                    },
                },
                {
                    'containerID': '...',
                    'image': 'docker.io/istio/proxyv2:1.12.2',
                    'imageID': '...',
                    'lastState': {},
                    'name': 'istio-proxy',
                    'ready': True,
                    'restartCount': 0,
                    'started': True,
                    'state': {
                        'running': {
                            'startedAt': '2022-09-15T00:18:41Z',
                        },
                    },
                },
            ],
            'phase': 'Running',
        },
    })
    with patch('kubitus_pod_cleaner_operator.handlers.kopf', autospec=True) as mocked_kopf:
        with patch('kubitus_pod_cleaner_operator.handlers.core_v1_api') as mocked_core_v1_api:
            with patch('kubitus_pod_cleaner_operator.handlers.stream') as mocked_stream:
                run_pod_event(body, logger)
                assert not mocked_kopf.mock_calls
                assert not mocked_core_v1_api.mock_calls
                assert not mocked_stream.mock_calls
    assert not logger.mock_calls
