# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from logging import Logger, LoggerAdapter
from typing import Any, Optional, Union

import kopf
from kubernetes.client.api import core_v1_api
from kubernetes.stream import stream


@kopf.on.event('', 'v1', 'pods', labels={'job-name': kopf.PRESENT})
def pod_event(
    body: kopf.Body,
    status: kopf.Status,
    name: Optional[str],
    namespace: Optional[str],
    logger: Union[Logger, LoggerAdapter],
    **_: Any,
) -> None:
    """
    On Pod event.

    Args:
        body: Full resource.
        status: Spec.
        name: Pod name.
        namespace: Pod namespace.
        logger: Logger.
        _: Extra args.
    """
    if status.get('phase', 'unknown') != 'Running':
        return

    istio_proxy_seen = False
    for container_status in status.get('containerStatuses', []):
        if container_status.get('name') == 'istio-proxy':
            istio_proxy_seen = True
            continue
        terminated = container_status.get('state', {}).get('terminated', {})
        if terminated.get('exitCode') != 0:
            return
    if not istio_proxy_seen:
        return

    logger.info('Stopping istio-proxy')
    kopf.info(body, reason='IstioProxyStopping', message='Stopping istio-proxy')
    exec_command = ['curl', '-X', 'POST', 'http://localhost:15000/quitquitquit']
    stream(
        core_v1_api.CoreV1Api().connect_get_namespaced_pod_exec,
        name,
        namespace,
        container='istio-proxy',
        command=exec_command,
        stderr=True,
        stdin=False,
        stdout=True,
        tty=False,
    )
    logger.info('Stopped istio-proxy')
    kopf.info(body, reason='IstioProxyStopped', message='Stopped istio-proxy')
