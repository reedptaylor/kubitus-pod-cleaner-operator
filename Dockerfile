FROM python:3.10.7@sha256:53e577204d362233ee92aeb5119449271f5eb24f99c61464efe9167ddbc8640f AS builder

COPY requirements.txt requirements.txt

RUN pip install --user -r requirements.txt

FROM python:3.10.7-slim@sha256:f2ee145f3bc4e061f8dfe7e6ebd427a410121495a0bd26e7622136db060c59e0

RUN adduser --uid 1000 --disabled-password --gecos '' kubitus-pod-cleaner-operator

COPY --from=builder --chown=kubitus-pod-cleaner-operator:kubitus-pod-cleaner-operator /root/.local /home/kubitus-pod-cleaner-operator/.local
COPY kubitus_pod_cleaner_operator kubitus_pod_cleaner_operator

USER 1000

ENV PATH=/home/kubitus-pod-cleaner-operator/.local/bin:$PATH

ENTRYPOINT ["kopf", "run", "--all-namespaces", "--liveness=http://0.0.0.0:8080/healthz", "kubitus_pod_cleaner_operator/handlers.py"]
