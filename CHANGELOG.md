# Change Log

### [1.0.4](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/compare/v1.0.3...v1.0.4) (2022-09-26)


### :repeat: Chore

* **deps:** pin dependencies ([6edfb2f](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/6edfb2f81c584ae059d3c22da29bec0dcff65f77))
* **deps:** pin dependencies ([a667bbd](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/a667bbd462448e330108bd26524c217ec2896fc5))
* **deps:** pin pep8 deps ([6807de8](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/6807de8bf5490f563a0e93c8531de724098e45dc))
* **deps:** pin pep8 deps ([de19348](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/de193485d986d97d9a9c7ff3020966867150314c))
* **deps:** update dependency flake8-bugbear to v22.8.22 ([59c4e9a](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/59c4e9a08ef68fc5b727c3fc8b4ea122d3ba0a10))
* **deps:** update dependency flake8-bugbear to v22.8.22 ([ce41641](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/ce416412b6210982c4d2b28f24bec8623d8db802))
* **deps:** update dependency flake8-bugbear to v22.8.23 ([5fef060](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/5fef060e3229bc17cf52c8a9c93707f63da1d60e))
* **deps:** update dependency flake8-bugbear to v22.8.23 ([91e838d](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/91e838dbfe37b477cc5befc77c573935ebeaa423))
* **deps:** update dependency flake8-bugbear to v22.9.11 ([c2a6355](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/c2a635531bfa8ee27cf2cdf4e6d3d341dd40fd46))
* **deps:** update dependency flake8-bugbear to v22.9.11 ([5ebc057](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/5ebc057a10ce5781ee746d1325e922e030e86b3c))
* **deps:** update dependency flake8-bugbear to v22.9.23 ([2757bfe](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/2757bfe1a6fdbf2aa7bfa1b212f3e878c89dd945))
* **deps:** update dependency flake8-bugbear to v22.9.23 ([7ad8f7a](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/7ad8f7afd743d32eb86294b65f117a3bb2eafb0c))
* **deps:** update dependency flake8-eradicate to v1.4.0 ([ca24988](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/ca24988f0b310e81d74183bda9ff1e76bd349dcf))
* **deps:** update dependency flake8-eradicate to v1.4.0 ([7e0468a](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/7e0468aa09337873e46778fce7dfa46d9f29bc18))
* **deps:** update dependency flake8-logging-format to v0.7.5 ([6cebd07](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/6cebd07f3c19511ee5b68e435c3cdd09a6a33a48))
* **deps:** update dependency flake8-logging-format to v0.7.5 ([b25e7bb](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/b25e7bb68f7dbd4a895526e20dc64d8b4f6d5fdc))
* **deps:** update dependency gcr.io/kaniko-project/executor to v1.9.0 ([c01739f](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/c01739fb0a0f7b0a8276b59536daac27961c626b))
* **deps:** update dependency gcr.io/kaniko-project/executor to v1.9.0 ([978d39f](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/978d39f0a512b5fa13c96b4aebd511b786db5da4))
* **deps:** update dependency helm/helm to v3.10.0 ([593106e](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/593106ea8161657fbbd56a7e0b3b61ff3fae69c6))
* **deps:** update dependency helm/helm to v3.10.0 ([922a306](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/922a306b0190465c08004e4e854ca6bedc3a237d))
* **deps:** update dependency helm/helm to v3.9.1 ([7e6d452](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/7e6d452b2de92bdc00d074d42c6e56c52d72e4b6))
* **deps:** update dependency helm/helm to v3.9.1 ([a52ea44](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/a52ea444ede9ec52ef1358b688fb26bbd332c6d0))
* **deps:** update dependency helm/helm to v3.9.2 ([e77d6d2](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/e77d6d2b5363a6dd761cfa4f955cfadbe17e3f96))
* **deps:** update dependency helm/helm to v3.9.2 ([4d5f7de](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/4d5f7de118bbd413c9070ca79ae66f7fe24c5176))
* **deps:** update dependency helm/helm to v3.9.3 ([680024d](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/680024d08159367c6f67ab4e26dc73cb298060e8))
* **deps:** update dependency helm/helm to v3.9.3 ([aa5765d](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/aa5765d3547e742e1eaf830208cfd9b85a28accc))
* **deps:** update dependency helm/helm to v3.9.4 ([29cd5ad](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/29cd5ad7040f8512dd8e388c83b167adb0fcc64d))
* **deps:** update dependency helm/helm to v3.9.4 ([c0d43fd](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/c0d43fd7c111e1c412a0d7221d8b0e7e4acece8f))
* **deps:** update dependency kopf to v1.35.6 ([38d54ee](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/38d54ee05b6fd247fbde67cc534e412fd4674b43))
* **deps:** update dependency kopf to v1.35.6 ([014d47b](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/014d47b9f308cb7052bb9202f6108a3b802588df))
* **deps:** update dependency pbr to v5.10.0 ([0a97030](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/0a970303a5bfdc484ed5559513a2d9043ac86de8))
* **deps:** update dependency pbr to v5.10.0 ([55df4a3](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/55df4a36107cb7b7069644787edb483ba9a4defd))
* **deps:** update dependency pygments to v2.13.0 ([9b0b8ef](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/9b0b8ef50f4eb1a6b6bd8d4dfc53466abff5178b))
* **deps:** update dependency pygments to v2.13.0 ([7142f71](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/7142f71c53b7b334db6050eda2fa32dd533f8b6a))
* **deps:** update dependency pylint to v2.15.0 ([d28151b](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/d28151b20c260b373df5a522f2d5a57d84806e89))
* **deps:** update dependency pylint to v2.15.0 ([175ea54](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/175ea54fd04ece17dbb8fa0330dc9c9d845b1c30))
* **deps:** update dependency pylint to v2.15.2 ([ea644f6](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/ea644f63e7ddaf446fb29763d519481532f48c22))
* **deps:** update dependency pylint to v2.15.2 ([4b206ce](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/4b206ce56a70c207536ed347d4f470e78b552393))
* **deps:** update dependency pylint to v2.15.3 ([9a425b5](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/9a425b50135bae8a1fe2984c21049f35d6ec620f))
* **deps:** update dependency pylint to v2.15.3 ([a10eb5d](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/a10eb5d0807cf9eaf9bfc4bcb26ed0846bbf72dd))
* **deps:** update dependency python to v3.10.6 ([8550c00](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/8550c0086725217237eb5f8e1c79c71ab423f7b7))
* **deps:** update dependency python to v3.10.6 ([bd7fb96](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/bd7fb9612180d575ee76da0a3d31df0f42708b5f))
* **deps:** update node.js to 7b51a31 ([f22e8da](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/f22e8da5c9d201a33ae83c8579ff569ec752f5b1))
* **deps:** update node.js to 7b51a31 ([5e02c65](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/5e02c65d6eb891231ed104bf375cf70bcdc74886))
* **deps:** update node.js to 8a45c95 ([ede3d89](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/ede3d8994910e7f0888c8576ec922709e465ed75))
* **deps:** update node.js to 8a45c95 ([65f47fc](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/65f47fc007659ab01d9384daf099469d46f0726f))
* **deps:** update node.js to 8d3001f ([a140071](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/a14007164cce9f274ee1aa52f88a3b7e61480693))
* **deps:** update node.js to 8d3001f ([24c1bb7](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/24c1bb781f342f85a4325760776cdb06bb045595))
* **deps:** update Node.js to v18.5.0 ([5cf6d58](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/5cf6d582b821669305e63cd8f0780f69138fa111))
* **deps:** update Node.js to v18.5.0 ([e123620](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/e123620600a8df20fb8a7dd01c32ceb23ca7a339))
* **deps:** update node.js to v18.6.0 ([689224e](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/689224e47683df9014785456065849ea16ac6956))
* **deps:** update node.js to v18.6.0 ([69e4026](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/69e402698dfc535471bbd80db39031d01ec6af98))
* **deps:** update node.js to v18.7.0 ([6f76dee](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/6f76dee76eec90d70c5387330a3b1efe4a95a485))
* **deps:** update node.js to v18.7.0 ([4264246](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/4264246424d5a617bbf3e503a21c365544b0d9e7))
* **deps:** update node.js to v18.8.0 ([d017ae5](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/d017ae5a0e4d229f6abaf6e16f183a228bfddba4))
* **deps:** update node.js to v18.8.0 ([e953eb7](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/e953eb7de8d6452b6feb336b9df13919a5d62cc1))
* **deps:** update node.js to v18.9.0 ([1765ff7](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/1765ff791ec9f7bf46f2035d583d890e73bfea61))
* **deps:** update node.js to v18.9.0 ([64a5cfc](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/64a5cfcffb1d1d811e88ef926a114af3ef04ff15))
* **deps:** update python digest to ac63ff0 ([66f1769](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/66f17696d73ff5e1eb8c9b339f24094b21555de3))
* **deps:** update python digest to ac63ff0 ([caa1afa](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/caa1afaa570ba5d201a44d9a8c326b54dbc18a6b))
* **deps:** update python digest to dff7fd9 ([d0cf723](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/d0cf723a38b6420a3095efeada7a9722cd63070b))
* **deps:** update python digest to dff7fd9 ([e445d03](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/e445d0333ac58c407f418e251817923ab19b81ca))
* **deps:** update python docker tag to v3.10.7 ([a838e2c](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/a838e2cce84209b1364516fdcb411b49ac049a7c))
* **deps:** update python docker tag to v3.10.7 ([5199e8c](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/5199e8c65b7aadde9302b1414282a983158fd4ce))
* **deps:** update python:3.10.7 docker digest to 5bbf8c1 ([7af1641](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/7af16412e1d83f9adb9960ed76b8c53d2e802e33))
* **deps:** update python:3.10.7 docker digest to 5bbf8c1 ([6683ea6](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/6683ea6b9b12f3f39926eebb65f963e4f3de0442))
* **deps:** update python:3.10.7 docker digest to b2fa80a ([9632729](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/9632729fbe7885ce548ed336635213a364252e1f))
* **deps:** update python:3.10.7 docker digest to b2fa80a ([38d3012](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/38d30122adad3bb8738c9009bae192908d58d97d))
* **deps:** update python:3.10.7 docker digest to e9c3553 ([78f5783](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/78f578342eed91cb0dd4a541a3b5d597085c0d3c))
* **deps:** update python:3.10.7 docker digest to e9c3553 ([a3aaeb7](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/a3aaeb75f5913bca1c8d7bc57db777a678bb9f9e))
* **deps:** update python:3.10.7 docker digest to fc1317a ([77a6b21](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/77a6b215a74b9a2b8e195acf461102d512dbf3a8))
* **deps:** update python:3.10.7 docker digest to fc1317a ([24f5b7e](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/24f5b7ebe03440da235a24ac8089e839d3e50cd8))
* **deps:** update python:3.10.7-slim docker digest to 6de22c9 ([3531277](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/353127756aa7a091b9ea92b47ec3374edf6987c1))
* **deps:** update python:3.10.7-slim docker digest to 6de22c9 ([22d9878](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/22d9878173475903b548d84a243c5d72eac3488b))
* **deps:** update python:3.10.7-slim docker digest to 7f1c01a ([f826cb9](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/f826cb986da23014d0c89bd7c20aeb84d794a8b7))
* **deps:** update python:3.10.7-slim docker digest to 7f1c01a ([5478f53](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/5478f53ee5ec04b22fd28e7a8ca42df71122d01e))
* **deps:** update python:3.10.7-slim docker digest to bbfaa9d ([69c709d](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/69c709da7d3fa15ffdbacd33f67d91062a959464))
* **deps:** update python:3.10.7-slim docker digest to bbfaa9d ([850058a](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/850058a8f818eef8155e20362d73f7a43b3f27c6))
* **deps:** update python:3.10.7-slim docker digest to c0a3f67 ([fe45259](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/fe45259607080b8dbf727fb4ea3acc1a5b346e58))
* **deps:** update python:3.10.7-slim docker digest to c0a3f67 ([deba7ac](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/deba7acc8bea4b4ef9f4a67b3d7cac831d9ad8db))


### :bug: Fixes

* return for Error and CrashLoopBackOff containers ([cb9296b](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/cb9296bd0e8408c5edf51b35b86b5a19b2483a01)), closes [#5](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/issues/5)
* return for waiting and running containers ([2becec2](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/2becec2c1c134b0fbe2dd987ca9b9bb600c62e28)), closes [#5](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/issues/5)

### [1.0.3](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/compare/v1.0.2...v1.0.3) (2022-06-29)


### :bug: Fixes

* skip pods without an "istio-proxy" container ([2f7570e](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/2f7570e1365f1763da6e3a0b54848171ff96139f))
* skip pods without an "istio-proxy" container ([e946b37](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/e946b379c5b5bb4187345665f6f14bf80a5c3555))


### :repeat: Chore

* **deps:** update dependency helm/helm to v3.9.0 ([5ef144e](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/5ef144ea1c8b126d5fb248fff77a7c9ad66032e2))
* **deps:** update dependency helm/helm to v3.9.0 ([4207433](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/4207433dca93cb4412525052f902c5db80a4259a))
* **deps:** update dependency kopf to v1.35.5 ([51fa893](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/51fa8937f5bf4abbb33d7a3fdf142c1ae46de00b))
* **deps:** update dependency kopf to v1.35.5 ([5f3d5b0](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/5f3d5b015400af40677dd0f57d28bc93380ceb18))
* **deps:** update dependency kubernetes to v23.6.0 ([420d566](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/420d566481e15ca8b474b74ad3b7b1f3b31b6227))
* **deps:** update dependency kubernetes to v23.6.0 ([6700760](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/67007607e75a3d9b5ba4e06ce643ac3d5ddd7a17))
* **deps:** update dependency kubernetes to v24 ([09bb564](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/09bb5643ee1794829fdc78864c91b8d5b197470f))
* **deps:** update dependency kubernetes to v24 ([7d0c8bd](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/7d0c8bdbe723bc93e49d4bc1252bc8bd956fba43))
* **deps:** update dependency python to v3.10.5 ([db2cbe0](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/db2cbe002b4571dfbc8ff07a9f385031e5ca52e6))
* **deps:** update dependency python to v3.10.5 ([86ea88b](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/86ea88bf1b435c5c42b350729eebf71053f39ada))
* **deps:** update node.js to v18.2.0 ([70e3182](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/70e3182f16d7912759924d910ed41b6443dfa29f))
* **deps:** update node.js to v18.2.0 ([c604a87](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/c604a87e7693ed0b89f5a41575047c3ff4ba71c5))
* **deps:** update node.js to v18.3.0 ([e86f34d](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/e86f34db2cf99a168cf5d50d76ab76bcc5cc21d6))
* **deps:** update node.js to v18.3.0 ([3b4b2b7](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/3b4b2b7e7df2f79fdc5971d6eabeb6beca334f79))
* **deps:** update node.js to v18.4.0 ([93f16a0](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/93f16a0d36a882023cb49d1a5332d6d1d7d029e6))
* **deps:** update node.js to v18.4.0 ([0ccdc99](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/0ccdc9910addd8e135d2d47ae2a4f1a46934a261))
* **license:** add Apache  License 2.0 ([27b0cde](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/27b0cde956b1f7549babcec7a10997f8ff288a29))
* **release:** add header to changelog ([7beed01](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/7beed012bee35e20f45aaf4c5eac194555e5d805))

### [1.0.2](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/compare/v1.0.1...v1.0.2) (2022-05-06)


### :repeat: Chore

* **semantic-release:** fix version on release ([75c54de](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/75c54de71518468d4cdbac2a7a8a2ea42294ff8d))

### [1.0.1](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/compare/v1.0.0...v1.0.1) (2022-05-06)


### :repeat: CI

* enforce commit convention ([431dc1c](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/431dc1cd7198e14f3799f6443695cb87de2f31e5))


### :question: Unclassified

* 1.0.1a0 ([be4a261](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/be4a261871531126f578e0990854a3d50e02892b))
* 1.0.1a0 ([5db9763](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/5db9763c4efec2e6a61180029d41c1a618416280))
* Semtantic commit and release ([d9da459](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/d9da459a9a579d5014bb0077ec46f6717f16586b))


### :repeat: Chore

* **deps:** update dependency gcr.io/kaniko-project/executor to v1.8.1 ([cfdb09c](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/cfdb09c4b32b2805dd0aa739a5f55f468a1f7481))
* **deps:** update dependency gcr.io/kaniko-project/executor to v1.8.1 ([7418154](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/74181546efe98e11702c44b55a0320690c22ae8f))
* **deps:** update dependency helm/helm to v3.8.2 ([b7f78af](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/b7f78affb7e149e4616fb714949e7f74c4d07607))
* **deps:** update dependency helm/helm to v3.8.2 ([e1746b5](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/e1746b5403f6610dde6b25da5ed00a768a6fc2e1))
* **deps:** update dependency kopf to v1.35.4 ([c6d16b9](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/c6d16b95abb09a49dec3ecfc7cccf105f084c30f))
* **deps:** update dependency kopf to v1.35.4 ([1dea94f](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/1dea94f25e1d651b5bcb7a1f0f40155e1eaa457b))
* **deps:** update dependency kubernetes to v23 ([1cf5461](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/1cf5461cbd679523ca623f78e536f7f61183c353))
* **deps:** update dependency kubernetes to v23 ([af12a53](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/af12a533659bd8e7bc4451dca1b6647f7da85821))
* **deps:** update dependency python to v3.10.4 ([bad8cc6](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/bad8cc65c2e1907c3e5260b300381d385c766960))
* **deps:** update dependency python to v3.10.4 ([6abcc1f](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/6abcc1fe09f7236708676fe8828774866fa146b5))
* **semantic-release:** update version on release ([0234a3c](https://gitlab.com/kubitus-project/kubitus-pod-cleaner-operator/commit/0234a3c6533f214f4ab907e213f7941342b4378d))
