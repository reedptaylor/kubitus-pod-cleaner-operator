# Install Kubitus Pod Cleaner Operator using Helm

## Installing the chart

```shell
helm repo add kubitus-pod-cleaner-operator https://gitlab.com/api/v4/projects/32151358/packages/helm/stable
helm install kubitus-pod-cleaner-operator kubitus-pod-cleaner-operator/kubitus-pod-cleaner-operator
```

Note: You can replace the `stable` channel above by any branch name
for developpement versions.

## Configuration

| Parameter                                       | Description                                   | Default                                                             |
|-------------------------------------------------|-----------------------------------------------|---------------------------------------------------------------------|
| `image.repository`                              | Image repository                              | `registry.gitlab.com/kubitus-project/kubitus-pod-cleaner-operator`  |
| `image.pullPolicy`                              | Image pull policy                             | `IfNotPresent`                                                      |
| `image.tag`                                     | Image tag                                     | `""` (`Chart.AppVersion`)                                           |
| `imagePullSecrets`                              | Image pull secrets                            | `[]`                                                                |
| `nameOverride`                                  | Override the deployment name                  | `""` (`Chart.Name`)                                                 |
| `fullnameOverride`                              | Override the deployment full name             | `""` (`Release.Namespace-Chart.Name`)                               |
| `serviceAccount.create`                         | Create service account                        | `true`                                                              |
| `serviceAccount.annotations`                    | ServiceAccount annotations                    | `{}`                                                                |
| `serviceAccount.name`                           | Service account name to use, when empty will be set to created account if `serviceAccount.create` is set else to `default` | `""` |
| `rbac.create`                                   | Create `ClusterRole` and `ClusterRoleBinding` | `true`                                                              |
| `podAnnotations`                                | Pod annotations                               | `{}`                                                                |
| `podSecurityContext`                            | Pod security context                          | `{}`                                                                |
| `securityContext`                               | Container security context                    | `{}`                                                                |
| `resources`                                     | CPU/Memory resource requests/limits           | `{}`                                                                |
| `nodeSelector`                                  | Node labels for pod assignment                | `{}`                                                                |
| `tolerations`                                   | Toleration labels for pod assignment          | `[]`                                                                |
| `affinity`                                      | Affinity settings for pod assignment          | `{}`                                                                |
